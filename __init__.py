#! /usr/bin/env python
# -*- coding: utf-8 -*-

from netcdf4 import NcDataset, NcGroup, NcVariable, NcDimension
from netcdf4 import (chartostring, date2index, date2num, getlibversion,
                     num2date, stringtoarr, stringtochar)
from utils import concatDimension
